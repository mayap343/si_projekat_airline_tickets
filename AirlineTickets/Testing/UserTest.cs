﻿using System;
using BusinessLayer.models;
using DataLayer;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Testing
{
    /* Klasa koja vrsi testiranje metoda vezanih za korisnike */
    /*
    * Spisak testiranih metoda:
    *  - InsertUser()
    *  - CheckUser()
    *  - GetAllUsers()
    */
    [TestClass]
    public class UserTest
    {
        /* --------------------------------------------------------*/
        /* Deklaracija globalnih promenljivih */
        private User user = new User();
        private AirlineRepository ar = new AirlineRepository();
        private Random rand = new Random();
        /* --------------------------------------------------------*/

        /* --------------------------------------------------------*/
        /* Inicijalizacija modela User - korisnika sa pocetnim podacima */
        [TestInitialize]
        public void Init()
        {
            user.address = "Test adresa korisnika"; // Dodela User modelu adrese korisnika
            user.email = "test@test.com"; // Dodela User modelu email adrese
            user.name = "ImeTest"; // Dodela User modelu imena
            user.password = "root"; // Dodela User modelu lozinke
            user.passport_number = rand.Next(1000000); // Generisanje nasumicnog broja pasosa
            user.phone = "00381222"; //  Dodela User modelu broj telefona
            user.surname = "PrezimeTest"; // Dodela User modelu prezimena
        }
        /* --------------------------------------------------------*/

        /* --------------------------------------------------------*/
        /* Testiranje metode InsertUser() koja vrsi dodavanje korisnika u bazu */
        [TestMethod]
        public void InsertUserTest()
        {
            bool found = false; // Pocetna inicijalizacija flega
            ar.InsertUser(user); // Poziv metode za dodavanje korisnika u bazu

            foreach (User u in ar.GetAllUsers()) // Prolazak kroz listu korisnika iz baze
            {
                if (u.passport_number == user.passport_number) // Provera da li je nadjen let
                    found = true; // Postavljanje flega
            }

            Assert.IsTrue(found); // Provera sa Assertom da li je dobijena vrednost ocekivana vrednost
        }
        /* --------------------------------------------------------*/

        /* --------------------------------------------------------*/
        /* Testiranje metode CheckUser() koja vrsi proveru da li postoji korisnik sa email adresom i sifrom u bazi */
        [TestMethod]
        public void CheckUserTest()
        {
            ar.InsertUser(user); // Dodavanje predhodno inicijalizovanog korisnika 
            User tempUser = new User(); 
            for (int i = 0; i < 2; i++) // Kreiranje jos 2 korisnika u bazi
            {
                tempUser.address = "Test adresa korisnika"+ rand.Next(100);// Dodela User modelu adrese korisnika
                tempUser.email = "test"+ rand.Next(100)+"@test.com";// Dodela User modelu email adrese
                tempUser.name = "ImeTest" + rand.Next(100);// Dodela User modelu imena
                tempUser.password = "root";// Dodela User modelu lozinke
                tempUser.passport_number = rand.Next(1000000);// Generisanje nasumicnog broja pasosa
                tempUser.phone = "003812" + rand.Next(100); //  Dodela User modelu broj telefona
                tempUser.surname = "PrezimeTest" + rand.Next(100);// Dodela User modelu prezimena
                ar.InsertUser(tempUser);
            }
            int checkUserCount = ar.CheckUser(user); // Poziv metode za proveru da li postoji korisnik sa email adresom i sifrom 

            Assert.AreNotEqual(0, checkUserCount); // Rezultat ne sme biti 0. Ako je 0, znaci a nije pronadjen korisnik sa tom email adresom i sifrom

        }
        /* --------------------------------------------------------*/

        /* --------------------------------------------------------*/
        /* Testiranje metode GetAllUsers() koja vrsi slektovanje svih korisnika iz baze podataka i vraca Listu korisnika */
        [TestMethod]
        public void GetAllUsersTest()
        {
            int counter = ar.GetAllUsers().Count; // Dobijanje pocetne vrednosti trenuog broja korisniak u bazi
            for (int i = 0; i < 2; i++) // Dodavanje jos 3 korisnika u bazu 
            {
                user.address = "Test adresa korisnika" + rand.Next(100);// Dodela User modelu adrese korisnika
                user.email = "test" + rand.Next(100) + "@test.com";// Dodela User modelu email adrese
                user.name = "ImeTest" + rand.Next(100);// Dodela User modelu imena
                user.password = "root";// Dodela User modelu lozinke
                user.passport_number = rand.Next(1000000);// Generisanje nasumicnog broja pasosa
                user.phone = "003812" + rand.Next(100); //  Dodela User modelu broj telefona
                user.surname = "PrezimeTest" + rand.Next(100);// Dodela User modelu prezimena
                ar.InsertUser(user); // Poziv metode za dodavanje korisnika u bazu 
                counter++; // Uvecavanje pocetnog brojaca
            }
            Assert.AreEqual(counter, ar.GetAllUsers().Count); // Provera sa Assertom da li ocekivana vrednost broja korisnika jednaka sa stvarnim brojem korisnika u bazi
        }
        /* --------------------------------------------------------*/
    }
}
