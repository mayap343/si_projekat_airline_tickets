﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.models
{
    public class CityPrice
    {
        public double Price { get; set; }
        public string CityFrom { get; set; }
        public string CityTo { get; set; }

        public CityPrice(double price, string cityFrom, string cityTo)
        {
            Price = price;
            CityFrom = cityFrom;
            CityTo = cityTo;
        }
    }
}
