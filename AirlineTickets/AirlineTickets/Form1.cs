﻿using BusinessLayer;
using BusinessLayer.models;
using DataLayer.models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AirlineTickets
{
    public partial class Form1 : Form
    {
        string trip;
        string typeOfClass; 
        private AirlineBusiness airlineBusiness;



        string currentCity = "";

        List<CityPrice> cityPrices;
        Login login;
        double totalPrice = 0;

        public Form1(Login login)
        {
            this.login = login;
            InitializeComponent();
            cityPrices = new List<CityPrice>();

            cityPrices.Add(new CityPrice(160, "Belgrade", "Basel"));
            cityPrices.Add(new CityPrice(200, "Belgrade", "New York"));
            cityPrices.Add(new CityPrice(260, "Belgrade", "Moscow"));
            cityPrices.Add(new CityPrice(300, "Belgrade", "Berlin"));
            cityPrices.Add(new CityPrice(260, "New York", "Basel"));
            cityPrices.Add(new CityPrice(210, "New York", "Berlin"));
            cityPrices.Add(new CityPrice(330, "New York", "Moscow"));
            cityPrices.Add(new CityPrice(120, "Basel", "Moscow"));
            cityPrices.Add(new CityPrice(220, "Basel", "Berlin"));
            cityPrices.Add(new CityPrice(440, "Berlin", "Moscow"));
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            
            monthCalendarArriving.SetDate(DateTime.Now);
            monthCalendarDeparture.SetDate(DateTime.Now);
            comboBoxFrom.ResetText();
            comboBoxTo.ResetText();
            numericUpDownChildren.ResetText();
            numericUpDownAdult.ResetText();

            radioButtonEconomy.Checked = true;
            radioButtonOneDirection.Checked = true;
            CurrentPrice.ResetText();
        }

        
        private void radioButtonBusiness_CheckedChanged_1(object sender, EventArgs e)
        {
            typeOfClass = "Business";
        }

        private void radioButtonEconomy_CheckedChanged_1(object sender, EventArgs e)
        {
            typeOfClass = "Economy";
            
        }

        private void radioButtonOneDirection_CheckedChanged_1(object sender, EventArgs e)
        {
            trip = "One direction";
            
        }

        private void radioButtonReturn_CheckedChanged_1(object sender, EventArgs e)
        {
            trip = "Return";
            
        }

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            
            Form SumOfInformations = new SumOfInformations(totalPrice, radioButtonReturn.Checked, radioButtonBusiness.Checked, monthCalendarDeparture, monthCalendarArriving); 
            SumOfInformations.ShowDialog();
            
        }

        private void buttonSignUp_Click(object sender, EventArgs e)
        {
            Signup sp = new Signup();
            sp.Show();
        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {
            Login l = new Login();
            l.Show();
            
        }

        private void comboBoxTo_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                // Int32 valueFrom = comboBoxFrom.SelectedIndex;
                if(currentCity != "")
                  comboBoxTo.Items.Add(currentCity);

                currentCity = comboBoxFrom.Text;
                comboBoxTo.Items.Remove(currentCity);

            }
            catch
            {
                
            }
        }


        private void monthCalendarArriving_DateSelected(object sender, DateRangeEventArgs e)
        {
            DateTime mdeparture = monthCalendarDeparture.SelectionStart;
            DateTime marriving = monthCalendarArriving.SelectionEnd;
            if (marriving < mdeparture)
            {
                MessageBox.Show("You can not enter arriving date before departure date!");
            }
            else
            {
               
            }
        }

        

        private void monthCalendarDeparture_DateChanged(object sender, DateRangeEventArgs e)
        {
            DateTime currentDate = DateTime.Now;
            DateTime mdeparture = monthCalendarDeparture.SelectionStart;


            if (mdeparture < currentDate)
            {
                MessageBox.Show("Departure date must be greater or equal than current!");
            }
            else
            {

            }
        }
        
        private void button1_Click(object sender, EventArgs e)
        {

            totalPrice = 0;

            string c1 = comboBoxFrom.Text;
            string c2 = comboBoxTo.Text;

            double price = GetPrice(c1, c2);

            if(radioButtonReturn.Checked)
            {
                price *= 1.7;
            }

            if(radioButtonBusiness.Checked)
            {
                price *= 1.2;
            }


            int adultNum = Convert.ToInt32(numericUpDownAdult.Value);
            if(adultNum > 0)
            {
                totalPrice += price * adultNum;
            }

            int childNum = Convert.ToInt32(numericUpDownChildren.Value);
            if (childNum > 0)
            {
                totalPrice += (price * 0.7 * childNum);
            }

            CurrentPrice.Text = totalPrice.ToString() + "$";
        }

        private double GetPrice(string c1, string c2)
        {
            foreach(CityPrice cp in cityPrices)
            {
                if((cp.CityFrom == c1 && cp.CityTo == c2) || (cp.CityFrom == c2 && cp.CityTo == c1))
                {
                    return cp.Price;
                }
            }
            return 0;
        }

        private void CurrentPrice_Click(object sender, EventArgs e)
        {

        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            login.Close();
        }

        private void monthCalendarArriving_DateChanged(object sender, DateRangeEventArgs e)
        {

        }

        private void comboBoxFrom_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
