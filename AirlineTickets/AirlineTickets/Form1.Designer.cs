﻿namespace AirlineTickets
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.labelDeparture = new System.Windows.Forms.Label();
            this.labelArrivinig = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.numericUpDownChildren = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownAdult = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.buttonSearch = new System.Windows.Forms.Button();
            this.comboBoxFrom = new System.Windows.Forms.ComboBox();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.comboBoxTo = new System.Windows.Forms.ComboBox();
            this.groupBoxClass = new System.Windows.Forms.GroupBox();
            this.radioButtonEconomy = new System.Windows.Forms.RadioButton();
            this.radioButtonBusiness = new System.Windows.Forms.RadioButton();
            this.groupBoxTrip = new System.Windows.Forms.GroupBox();
            this.radioButtonReturn = new System.Windows.Forms.RadioButton();
            this.radioButtonOneDirection = new System.Windows.Forms.RadioButton();
            this.buttonSignUp = new System.Windows.Forms.Button();
            this.monthCalendarDeparture = new System.Windows.Forms.MonthCalendar();
            this.monthCalendarArriving = new System.Windows.Forms.MonthCalendar();
            this.buttonLogin = new System.Windows.Forms.Button();
            this.CurrentPrice = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.panelPrice = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownChildren)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAdult)).BeginInit();
            this.groupBoxClass.SuspendLayout();
            this.groupBoxTrip.SuspendLayout();
            this.panelPrice.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelDeparture
            // 
            this.labelDeparture.AutoSize = true;
            this.labelDeparture.BackColor = System.Drawing.Color.Black;
            this.labelDeparture.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDeparture.ForeColor = System.Drawing.Color.White;
            this.labelDeparture.Location = new System.Drawing.Point(324, 22);
            this.labelDeparture.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelDeparture.Name = "labelDeparture";
            this.labelDeparture.Size = new System.Drawing.Size(82, 18);
            this.labelDeparture.TabIndex = 2;
            this.labelDeparture.Text = "Departure";
            // 
            // labelArrivinig
            // 
            this.labelArrivinig.AutoSize = true;
            this.labelArrivinig.BackColor = System.Drawing.Color.Black;
            this.labelArrivinig.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelArrivinig.ForeColor = System.Drawing.Color.White;
            this.labelArrivinig.Location = new System.Drawing.Point(591, 21);
            this.labelArrivinig.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelArrivinig.Name = "labelArrivinig";
            this.labelArrivinig.Size = new System.Drawing.Size(95, 18);
            this.labelArrivinig.TabIndex = 3;
            this.labelArrivinig.Text = "Return date";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Black;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(814, 173);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 18);
            this.label3.TabIndex = 6;
            this.label3.Text = "Flying from";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Black;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(814, 219);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 18);
            this.label4.TabIndex = 7;
            this.label4.Text = "Flying to";
            // 
            // numericUpDownChildren
            // 
            this.numericUpDownChildren.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.numericUpDownChildren.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.numericUpDownChildren.Location = new System.Drawing.Point(939, 42);
            this.numericUpDownChildren.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownChildren.Name = "numericUpDownChildren";
            this.numericUpDownChildren.Size = new System.Drawing.Size(150, 26);
            this.numericUpDownChildren.TabIndex = 14;
            // 
            // numericUpDownAdult
            // 
            this.numericUpDownAdult.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.numericUpDownAdult.Location = new System.Drawing.Point(939, 94);
            this.numericUpDownAdult.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownAdult.Name = "numericUpDownAdult";
            this.numericUpDownAdult.Size = new System.Drawing.Size(150, 26);
            this.numericUpDownAdult.TabIndex = 15;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Black;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(814, 48);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(70, 18);
            this.label5.TabIndex = 16;
            this.label5.Text = "Children";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Black;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(814, 100);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(45, 18);
            this.label6.TabIndex = 17;
            this.label6.Text = "Adult";
            // 
            // buttonSearch
            // 
            this.buttonSearch.BackColor = System.Drawing.Color.DarkRed;
            this.buttonSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSearch.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.buttonSearch.Location = new System.Drawing.Point(1079, 559);
            this.buttonSearch.Margin = new System.Windows.Forms.Padding(4);
            this.buttonSearch.Name = "buttonSearch";
            this.buttonSearch.Size = new System.Drawing.Size(217, 47);
            this.buttonSearch.TabIndex = 18;
            this.buttonSearch.Text = "Search tickets";
            this.buttonSearch.UseVisualStyleBackColor = false;
            this.buttonSearch.Click += new System.EventHandler(this.buttonSearch_Click);
            // 
            // comboBoxFrom
            // 
            this.comboBoxFrom.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.comboBoxFrom.FormattingEnabled = true;
            this.comboBoxFrom.Items.AddRange(new object[] {
            "Basel",
            "Belgrade",
            "Berlin",
            "Moscow",
            "New York"});
            this.comboBoxFrom.Location = new System.Drawing.Point(939, 165);
            this.comboBoxFrom.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxFrom.Name = "comboBoxFrom";
            this.comboBoxFrom.Size = new System.Drawing.Size(230, 26);
            this.comboBoxFrom.Sorted = true;
            this.comboBoxFrom.TabIndex = 22;
            this.comboBoxFrom.SelectedIndexChanged += new System.EventHandler(this.comboBoxFrom_SelectedIndexChanged);
            // 
            // btnRefresh
            // 
            this.btnRefresh.BackColor = System.Drawing.Color.Navy;
            this.btnRefresh.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRefresh.ForeColor = System.Drawing.Color.White;
            this.btnRefresh.Location = new System.Drawing.Point(13, 136);
            this.btnRefresh.Margin = new System.Windows.Forms.Padding(4);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(160, 47);
            this.btnRefresh.TabIndex = 25;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseVisualStyleBackColor = false;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // comboBoxTo
            // 
            this.comboBoxTo.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.comboBoxTo.FormattingEnabled = true;
            this.comboBoxTo.Items.AddRange(new object[] {
            "Basel",
            "Belgrade",
            "Berlin",
            "Moscow",
            "New York"});
            this.comboBoxTo.Location = new System.Drawing.Point(939, 211);
            this.comboBoxTo.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxTo.Name = "comboBoxTo";
            this.comboBoxTo.Size = new System.Drawing.Size(230, 26);
            this.comboBoxTo.Sorted = true;
            this.comboBoxTo.TabIndex = 26;
            this.comboBoxTo.MouseClick += new System.Windows.Forms.MouseEventHandler(this.comboBoxTo_MouseClick);
            // 
            // groupBoxClass
            // 
            this.groupBoxClass.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.groupBoxClass.Controls.Add(this.radioButtonEconomy);
            this.groupBoxClass.Controls.Add(this.radioButtonBusiness);
            this.groupBoxClass.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxClass.Location = new System.Drawing.Point(767, 286);
            this.groupBoxClass.Margin = new System.Windows.Forms.Padding(4);
            this.groupBoxClass.Name = "groupBoxClass";
            this.groupBoxClass.Padding = new System.Windows.Forms.Padding(4);
            this.groupBoxClass.Size = new System.Drawing.Size(445, 83);
            this.groupBoxClass.TabIndex = 28;
            this.groupBoxClass.TabStop = false;
            this.groupBoxClass.Text = "Class";
            // 
            // radioButtonEconomy
            // 
            this.radioButtonEconomy.AutoSize = true;
            this.radioButtonEconomy.Checked = true;
            this.radioButtonEconomy.Location = new System.Drawing.Point(309, 27);
            this.radioButtonEconomy.Margin = new System.Windows.Forms.Padding(4);
            this.radioButtonEconomy.Name = "radioButtonEconomy";
            this.radioButtonEconomy.Size = new System.Drawing.Size(90, 22);
            this.radioButtonEconomy.TabIndex = 15;
            this.radioButtonEconomy.TabStop = true;
            this.radioButtonEconomy.Text = "Economy";
            this.radioButtonEconomy.UseVisualStyleBackColor = true;
            this.radioButtonEconomy.CheckedChanged += new System.EventHandler(this.radioButtonEconomy_CheckedChanged_1);
            // 
            // radioButtonBusiness
            // 
            this.radioButtonBusiness.AutoSize = true;
            this.radioButtonBusiness.Location = new System.Drawing.Point(125, 27);
            this.radioButtonBusiness.Margin = new System.Windows.Forms.Padding(4);
            this.radioButtonBusiness.Name = "radioButtonBusiness";
            this.radioButtonBusiness.Size = new System.Drawing.Size(87, 22);
            this.radioButtonBusiness.TabIndex = 14;
            this.radioButtonBusiness.TabStop = true;
            this.radioButtonBusiness.Text = "Business";
            this.radioButtonBusiness.UseVisualStyleBackColor = true;
            this.radioButtonBusiness.CheckedChanged += new System.EventHandler(this.radioButtonBusiness_CheckedChanged_1);
            // 
            // groupBoxTrip
            // 
            this.groupBoxTrip.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.groupBoxTrip.Controls.Add(this.radioButtonReturn);
            this.groupBoxTrip.Controls.Add(this.radioButtonOneDirection);
            this.groupBoxTrip.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxTrip.Location = new System.Drawing.Point(125, 286);
            this.groupBoxTrip.Margin = new System.Windows.Forms.Padding(4);
            this.groupBoxTrip.Name = "groupBoxTrip";
            this.groupBoxTrip.Padding = new System.Windows.Forms.Padding(4);
            this.groupBoxTrip.Size = new System.Drawing.Size(445, 83);
            this.groupBoxTrip.TabIndex = 29;
            this.groupBoxTrip.TabStop = false;
            this.groupBoxTrip.Text = "Trip";
            // 
            // radioButtonReturn
            // 
            this.radioButtonReturn.AutoSize = true;
            this.radioButtonReturn.Location = new System.Drawing.Point(327, 29);
            this.radioButtonReturn.Margin = new System.Windows.Forms.Padding(4);
            this.radioButtonReturn.Name = "radioButtonReturn";
            this.radioButtonReturn.Size = new System.Drawing.Size(70, 22);
            this.radioButtonReturn.TabIndex = 14;
            this.radioButtonReturn.TabStop = true;
            this.radioButtonReturn.Text = "Return";
            this.radioButtonReturn.UseVisualStyleBackColor = true;
            this.radioButtonReturn.CheckedChanged += new System.EventHandler(this.radioButtonReturn_CheckedChanged_1);
            // 
            // radioButtonOneDirection
            // 
            this.radioButtonOneDirection.AutoSize = true;
            this.radioButtonOneDirection.Checked = true;
            this.radioButtonOneDirection.Location = new System.Drawing.Point(98, 29);
            this.radioButtonOneDirection.Margin = new System.Windows.Forms.Padding(4);
            this.radioButtonOneDirection.Name = "radioButtonOneDirection";
            this.radioButtonOneDirection.Size = new System.Drawing.Size(114, 22);
            this.radioButtonOneDirection.TabIndex = 13;
            this.radioButtonOneDirection.TabStop = true;
            this.radioButtonOneDirection.Text = "One direction";
            this.radioButtonOneDirection.UseVisualStyleBackColor = true;
            this.radioButtonOneDirection.CheckedChanged += new System.EventHandler(this.radioButtonOneDirection_CheckedChanged_1);
            // 
            // buttonSignUp
            // 
            this.buttonSignUp.BackColor = System.Drawing.Color.Navy;
            this.buttonSignUp.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSignUp.ForeColor = System.Drawing.Color.White;
            this.buttonSignUp.Location = new System.Drawing.Point(13, 27);
            this.buttonSignUp.Margin = new System.Windows.Forms.Padding(4);
            this.buttonSignUp.Name = "buttonSignUp";
            this.buttonSignUp.Size = new System.Drawing.Size(160, 39);
            this.buttonSignUp.TabIndex = 30;
            this.buttonSignUp.Text = "Sign up";
            this.buttonSignUp.UseVisualStyleBackColor = false;
            this.buttonSignUp.Click += new System.EventHandler(this.buttonSignUp_Click);
            // 
            // monthCalendarDeparture
            // 
            this.monthCalendarDeparture.BackColor = System.Drawing.Color.DarkSalmon;
            this.monthCalendarDeparture.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.monthCalendarDeparture.Location = new System.Drawing.Point(256, 58);
            this.monthCalendarDeparture.Margin = new System.Windows.Forms.Padding(11, 13, 11, 13);
            this.monthCalendarDeparture.Name = "monthCalendarDeparture";
            this.monthCalendarDeparture.TabIndex = 32;
            this.monthCalendarDeparture.TitleBackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.monthCalendarDeparture.TrailingForeColor = System.Drawing.SystemColors.ControlDark;
            this.monthCalendarDeparture.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.monthCalendarDeparture_DateChanged);
            // 
            // monthCalendarArriving
            // 
            this.monthCalendarArriving.Location = new System.Drawing.Point(516, 58);
            this.monthCalendarArriving.Margin = new System.Windows.Forms.Padding(11, 13, 11, 13);
            this.monthCalendarArriving.Name = "monthCalendarArriving";
            this.monthCalendarArriving.TabIndex = 33;
            this.monthCalendarArriving.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.monthCalendarArriving_DateChanged);
            this.monthCalendarArriving.DateSelected += new System.Windows.Forms.DateRangeEventHandler(this.monthCalendarArriving_DateSelected);
            // 
            // buttonLogin
            // 
            this.buttonLogin.BackColor = System.Drawing.Color.Navy;
            this.buttonLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonLogin.ForeColor = System.Drawing.Color.White;
            this.buttonLogin.Location = new System.Drawing.Point(13, 74);
            this.buttonLogin.Margin = new System.Windows.Forms.Padding(4);
            this.buttonLogin.Name = "buttonLogin";
            this.buttonLogin.Size = new System.Drawing.Size(160, 37);
            this.buttonLogin.TabIndex = 31;
            this.buttonLogin.Text = "Log in";
            this.buttonLogin.UseVisualStyleBackColor = false;
            this.buttonLogin.Click += new System.EventHandler(this.buttonLogin_Click);
            // 
            // CurrentPrice
            // 
            this.CurrentPrice.AutoSize = true;
            this.CurrentPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CurrentPrice.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.CurrentPrice.Location = new System.Drawing.Point(51, 90);
            this.CurrentPrice.Name = "CurrentPrice";
            this.CurrentPrice.Size = new System.Drawing.Size(0, 33);
            this.CurrentPrice.TabIndex = 34;
            this.CurrentPrice.Click += new System.EventHandler(this.CurrentPrice_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.MediumBlue;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.button1.Location = new System.Drawing.Point(23, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(150, 37);
            this.button1.TabIndex = 35;
            this.button1.Text = "Total Price";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // panelPrice
            // 
            this.panelPrice.BackColor = System.Drawing.Color.Transparent;
            this.panelPrice.Controls.Add(this.button1);
            this.panelPrice.Controls.Add(this.CurrentPrice);
            this.panelPrice.Location = new System.Drawing.Point(571, 400);
            this.panelPrice.Name = "panelPrice";
            this.panelPrice.Size = new System.Drawing.Size(196, 149);
            this.panelPrice.TabIndex = 36;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1319, 619);
            this.Controls.Add(this.panelPrice);
            this.Controls.Add(this.monthCalendarArriving);
            this.Controls.Add(this.monthCalendarDeparture);
            this.Controls.Add(this.buttonLogin);
            this.Controls.Add(this.buttonSignUp);
            this.Controls.Add(this.groupBoxTrip);
            this.Controls.Add(this.groupBoxClass);
            this.Controls.Add(this.comboBoxTo);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.comboBoxFrom);
            this.Controls.Add(this.buttonSearch);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.numericUpDownAdult);
            this.Controls.Add(this.numericUpDownChildren);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.labelArrivinig);
            this.Controls.Add(this.labelDeparture);
            this.Font = new System.Drawing.Font("Microsoft JhengHei", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.Black;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "AirlineTickets";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownChildren)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAdult)).EndInit();
            this.groupBoxClass.ResumeLayout(false);
            this.groupBoxClass.PerformLayout();
            this.groupBoxTrip.ResumeLayout(false);
            this.groupBoxTrip.PerformLayout();
            this.panelPrice.ResumeLayout(false);
            this.panelPrice.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label labelDeparture;
        private System.Windows.Forms.Label labelArrivinig;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown numericUpDownChildren;
        private System.Windows.Forms.NumericUpDown numericUpDownAdult;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button buttonSearch;
        private System.Windows.Forms.ComboBox comboBoxFrom;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.ComboBox comboBoxTo;
        private System.Windows.Forms.GroupBox groupBoxClass;
        private System.Windows.Forms.RadioButton radioButtonEconomy;
        private System.Windows.Forms.RadioButton radioButtonBusiness;
        private System.Windows.Forms.GroupBox groupBoxTrip;
        private System.Windows.Forms.RadioButton radioButtonReturn;
        private System.Windows.Forms.RadioButton radioButtonOneDirection;
        private System.Windows.Forms.Button buttonSignUp;
        private System.Windows.Forms.MonthCalendar monthCalendarDeparture;
        private System.Windows.Forms.MonthCalendar monthCalendarArriving;
        private System.Windows.Forms.Button buttonLogin;
        private System.Windows.Forms.Label CurrentPrice;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panelPrice;
    }
}

