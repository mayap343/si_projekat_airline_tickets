﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.models
{
   public class Flight
    {
        public int id_flight { get; set; }
        public DateTime arrival_date { get; set; }
        public DateTime departure_date { get; set; }
        public string flying_from { get; set; }
        public string flying_to { get; set; }

    }
}
