﻿using BusinessLayer.models;

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class AirlineRepository
    {
       
        private string connstr = "Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=AirlineTickets;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
       
        public List<Flight> GetAllFlights()
        {
            using (SqlConnection sqlConnection = new SqlConnection(connstr))
            {
                sqlConnection.Open();

                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandText = "SELECT * FROM Flights";

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                List<Flight> listOfFlights = new List<Flight>();

                while (sqlDataReader.Read())
                {


                    Flight f = new Flight();
                    f.id_flight = sqlDataReader.GetInt32(0);
                    f.arrival_date = sqlDataReader.GetDateTime(1);
                    f.departure_date = sqlDataReader.GetDateTime(2);
                    f.flying_from = sqlDataReader.GetString(3);
                    f.flying_to = sqlDataReader.GetString(4);


                    listOfFlights.Add(f);
                }

                return listOfFlights;
            }
        }
        public List<User> GetAllUsers()
        {
            using (SqlConnection sqlConnection = new SqlConnection(connstr))
            {
                sqlConnection.Open();

                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandText = "SELECT * FROM Users";

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                List<User> listOfUsers = new List<User>();

                while (sqlDataReader.Read())
                {


                    User u = new User();

                    u.passport_number = sqlDataReader.GetInt32(0);
                    u.name = sqlDataReader.GetString(1);
                    u.surname = sqlDataReader.GetString(2);
                    u.phone = sqlDataReader.GetString(3);
                    u.address = sqlDataReader.GetString(4);
                    u.email = sqlDataReader.GetString(5);
                    u.password = sqlDataReader.GetString(6);

                    listOfUsers.Add(u);
                }

                return listOfUsers;
            }
        }

        public int InsertFlight(Flight f)
        {
            using (SqlConnection sqlcon=new SqlConnection(connstr))
            {
                sqlcon.Open();

                SqlCommand sqlcom = new SqlCommand();
                sqlcom.Connection = sqlcon;
                sqlcom.CommandText = "INSERT INTO Flights VALUES(" + string.Format("'{0}','{1}','{2}','{3}','{4}'",f.id_flight,f.arrival_date,f.departure_date,f.flying_from,f.flying_to)+")";

                return sqlcom.ExecuteNonQuery();
            }
        }
       public int InsertUser(User u)
        {
            using (SqlConnection sqlcon=new SqlConnection(connstr))
            {
                sqlcon.Open();
                SqlCommand sqlcom = new SqlCommand();
                sqlcom.Connection = sqlcon;
                sqlcom.CommandText = "INSERT INTO Users VALUES(" + string.Format("'{0}','{1}','{2}','{3}','{4}','{5}','{6}'", u.passport_number, u.name, u.surname, u.phone, u.address, u.email, u.password) + ")";
                try
                { 
                    return sqlcom.ExecuteNonQuery();
                }
                catch
                {
                    return 0;
                }
            }

        }

        public int CheckUser(User u)
        {using (SqlConnection sqlcon=new SqlConnection(connstr))
            {
                sqlcon.Open();
                SqlCommand sqlcom = new SqlCommand();
                sqlcom.Connection = sqlcon;
                sqlcom.CommandText = "SELECT COUNT(*) FROM Users WHERE Email='"+u.email+"' AND Password='"+u.password+"'";
                return sqlcom.ExecuteNonQuery();
            }

        }
    }
}
