﻿using System;
using BusinessLayer.models;
using DataLayer;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Testing

{   /* Klasa koja vrsi testiranje metoda vezanih za letove */ 
    /*
     * Spisak testiranih metoda:
     *  - InsertFlight()
     *  - GetAllFlights()
    */

    [TestClass]
    public class FlightsTest
    {
        /* --------------------------------------------------------*/
        /* ---------- Deklaracija globalnih promenljivih ----------*/
        private AirlineRepository ar = new AirlineRepository();
        private Random rand = new Random();
        private Flight f = new Flight();
        private int flightID;
        /* --------------------------------------------------------*/

        /* --------------------------------------------------------*/
        /* Inicijalizacija modela Flight - let sa pocetnim podacima */
        [TestInitialize]
        public void Init()
        {
            flightID = rand.Next(1000); // Generisanje random ID-a
            f.id_flight = flightID; // Dodela Flight modelu ID leta
            f.arrival_date = DateTime.Now; // Dodela Flight modelu vreme polaska 
            f.departure_date = DateTime.Now;// Dodela Flight modelu  vreme poletanja
            f.flying_from = "Basel";// Dodela Flight modelu lokaciju polaska
            f.flying_to = "Berlin";// Dodela Flight modelu lokaciju odredista
        }
        /* --------------------------------------------------------*/

        /* --------------------------------------------------------*/
        /* Testiranje metode InsertFlight() koja vrsi dodavanje leta u bazu */
        [TestMethod]
        public void InsertFlightTest()
        {
            bool found = false; // Pocetna inicijalizacija flega
            ar.InsertFlight(f); // Poziv metode za dodavanje leta u bazu
            foreach (Flight fl in ar.GetAllFlights()) // Prolazak kroz listu korisnika iz baze
            {
                if (flightID == fl.id_flight) // Provera da li je nadjen let
                    found = true; // Ako jeste setujemo fleg
            }

            Assert.IsTrue(found); // Provera sa Assertom da li je dobijena vrednost ocekivana vrednost
        }
        /* --------------------------------------------------------*/

        /* --------------------------------------------------------*/
        /* Testiranje metode GetAllFlight() koja vrsi slektovanje svih letova iz baze podataka i vraca Listu letova */
        [TestMethod]
        public void GetAllFlightTest()
        {
            int counter = ar.GetAllFlights().Count; // Dobijanje pocetne vrednosti trenuog broja letova u bazi
            for (int i = 0; i < 2; i++) // Dodavanje jos 3 leta u bazu 
            {
                flightID = rand.Next(1000);// Generisanje random ID-a
                f.id_flight = flightID;// Dodela Flight modelu ID leta
                f.arrival_date = DateTime.Now;// Dodela Flight modelu vreme polaska 
                f.departure_date = DateTime.Now;// Dodela Flight modelu  vreme poletanja
                f.flying_from = "Basel"+ rand.Next(1000);// Dodela Flight modelu lokaciju polaska
                f.flying_to = "Berlin"+ rand.Next(1000);// Dodela Flight modelu lokaciju odredista
                ar.InsertFlight(f); // Poziv metode za dodavanje leta u bazu 
                counter++; // Uvecavanje pocetnog brojaca
            }

            Assert.AreEqual(counter,ar.GetAllFlights().Count); // Provera sa Assertom da li ocekivana vrednost broja letova je jednaka sa stvarnim brojem letova u bazi
        }
        /* --------------------------------------------------------*/
    }
}
