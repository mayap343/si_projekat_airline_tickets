﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.models
{
    public class Ticket
    {
       public int id_tickets { get; set; }
        public string price { get; set; }
        public double discount { get; set; }
        public string classAE{ get; set; }
        public  int flight_id { get; set; }
        public string returno { get; set; }
        public int passport_number { get; set; }
    }
}
