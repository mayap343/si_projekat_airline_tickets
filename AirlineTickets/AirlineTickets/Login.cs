﻿using BusinessLayer;
using BusinessLayer.models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AirlineTickets
{
    public partial class Login : Form
    {
        private AirlineBusiness airlineBusiness;
        public Login()
        {
            InitializeComponent();
            airlineBusiness = new AirlineBusiness();
        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {
            User u = new User();
           
            u.email = textBoxEmail.Text;
            u.password = textBoxPassword.Text;
            bool result = this.airlineBusiness.CheckUser(u);

            if (result)
            {
                Form1 f1 = new Form1(this);
                f1.Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("Wrong password or email!");
            }
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Signup signup = new Signup();
            signup.ShowDialog();
        }

        private void textBoxPassword_TextChanged(object sender, EventArgs e)
        {
            textBoxPassword.PasswordChar = '●';
        }

        
    }
}
