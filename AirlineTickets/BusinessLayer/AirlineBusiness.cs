﻿using BusinessLayer.models;
using DataLayer;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class AirlineBusiness
    {
        private AirlineRepository airlineRepository;

        public AirlineBusiness()
        {
            this.airlineRepository = new AirlineRepository();
        }

      

        public List<Flight> GetAllFlights()
        {
            return this.airlineRepository.GetAllFlights();
        }

        public List<User> GetAllUsers()
        {
            return this.airlineRepository.GetAllUsers();
        }


        public bool InsertFlight(Flight f)
        {
            int result = 0;
            if (f != null)
            {
                result = this.airlineRepository.InsertFlight(f);
            }
            if (result > 0)
            {
                return true;
                
            }
            return false;
        }

        public bool InsertUser(User u)
        {
            int result = 0;
            if (u != null)
            {
                result = this.airlineRepository.InsertUser(u);
            }
            if (result > 0)
            {
                return true;
            }
            return false;
        }
        public bool CheckUser(User u)
        {

            List<User> userList = airlineRepository.GetAllUsers();

            foreach(User user in userList)
            {
                if(user.password==u.password && user.email == u.email)
                {
                    return true;
                }
            }
            return false;
        }



    }

}
