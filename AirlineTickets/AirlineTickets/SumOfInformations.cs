﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AirlineTickets
{
    public partial class SumOfInformations : Form
    {
        public SumOfInformations(double totalPrice, bool isReturn, bool isBusiness, MonthCalendar monthCalendarDeparture, MonthCalendar monthCalendarArriving)
        {
            InitializeComponent();

            lblTotal.Text = totalPrice.ToString() + "$";

            lblDeparture.Text = monthCalendarDeparture.SelectionRange.Start.ToShortDateString();
            lblArrival.Text = monthCalendarArriving.SelectionRange.Start.ToShortDateString();


            if (isReturn == true)
            {
                lblType.Text = "Return";
            }
            else
            {
                lblType.Text = "One direction";
                lblArrival.Hide();
            }

            if (isBusiness == true)
            {
                lblClass.Text = "Business class";
            }
            else
            {
                lblClass.Text = "Economy class";
            }


        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Ticket bought succesfully!");
        }
    }
}
